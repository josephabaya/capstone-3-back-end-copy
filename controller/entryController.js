const Entry = require('./../models/entryModel')


// // module.exports.addTransaction = (req,res) => {
// 	// res.send("newTransaction")
// 	let newTransaction = new Entry({
// 		category : req.body.id,
// 		transaction : req.body.transaction,
// 		amount : req.body.amount
// 	})
// 	newTransaction.save()
// 	.then(() => res.send('Successfully added'))
// 	.catch(() => res.send(false))
// // }

module.exports.addTransaction = (req,res,next) => {
	
	// Entry.create(req.body)
	// .then( entry => {
	// 	res.send(entry)
	// })
	// .catch(next)

	let newEntry = new Entry({
		category : req.body.category,
		transaction : req.body.transaction,
		amount : req.body.amount,
		userId : req.user.id
	})
	newEntry.save()									
	.then(() => res.send('Successfully added'))
	.catch(() => res.send(false))

}




module.exports.getAllExpenses = (req,res) => {
	Entry.find({ userId: req.user.id, "transaction" : "Expenses"})
	// Entry.findById(req.params.id)
	//Entry.find({userId: req.params.userId}, {"$set":{transactions: "Expenses"}})
	.then(entry =>{
		res.send(entry)
	})
	.catch( err =>res.send(err))

	// let findAll = {
	// 	Expenses : true
	// }
	// Entry.FindById(req.params.id, findAll)
	// .then(() => res.send(true))				//error findbyId is not a function
	// .catch( () => res.send(false))

}


module.exports.getSingleExpenses = (req,res,next) => {
	
    Entry.findById(req.params.id)
    .then( Entry =>{
        res.send(Entry)
    })
    .catch(next)
}





module.exports.getAllIncome = (req,res) => {
	Entry.find({ userId: req.user.id, "transaction" : "Income"})
	.then(entry =>{
		res.send(entry)
	})
	.catch( err =>res.send(err))
}


module.exports.getSingleIncome = (req,res,next) => {
	
    Entry.findById(req.params.id)
    .then( Entry =>{
        res.send(Entry)
    })
    .catch(next)
}



module.exports.updatedTransaction = (req,res,next) => {
	
	// let updatedTransaction = {
	//     category : req.body.category,
	//     transaction : req.body.transaction,
	//     amount : req.body.amount,
	//     userID : req.body.userID
	// }
	
	// Entry.findByIdAndUpdate(req.params.entryId, updatedTransaction) 
	// .then( () => res.send(true) )
	// .catch( () => res.send(false))

	Entry.findByIdAndUpdate(req.params.id, req.body, {new: true})
	    .then( entry =>{
	        res.send(entry)
    })
    .catch(next)

}



module.exports.deleteEntry = (req,res) => {
	Entry.findByIdAndDelete(req.params.id)
	.then((entry) => {
		res.send(entry)
	})
	.catch((err)=>{
		res.send(err)
	})
}















// Sir  harry backend

// router.get('/', login, (req, res, next) => {
//     Income.find({userId: req.user.userId})
//     .then( income => res.send(income))
//     .catch(next)
// })

// router.get('/', login, (req, res, next) => {
//     Expense.find({userId: req.user.userId})
//     .then( expenses => res.send(expenses))
//     .catch(next)
// })


//Entry.find({Category: req.body.Category}, {"$set":{transactions: "Expenses"}})




// router.put('/:id', login, (req, res, next) => {
//     Income.findById(req.params.id)
//     .then( income => {
//         if (req.user.userId !== income.userId){
//                 res.status(401)
//                 throw new Error("Please login to your account to update your Income item.")
//         } else {

//             Income.findByIdAndUpdate(req.params.id, {$set:req.body}, {new: true})
//             .then( income => {
//                 res.send(income)
//             })
//             .catch(next)
//             }
//     })
//     .catch(next)
// })





