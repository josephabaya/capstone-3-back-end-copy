const Category = require('./../models/categoryModel')


module.exports.addCategory = (req,res,next) => {
	// Category.create({userId:req.user.id})
 //    .then( category => {
 //        res.send(category)
 //    })
 //    .catch(next)

	let newCategory = new Category({
		category : req.body.category,
		description : req.body.description,
		userId : req.user.id
	})
	newCategory.save()
	.then(() => res.send('Successfully added'))
	.catch(() => res.send(false))
}


module.exports.viewAllCategory = (req,res) => {
	
    // Category.find({userId: req.params.userId})
    // .then( category =>{
    //     res.send(category)
    // })
    // .catch(err=>res.send(err))

    Category.find({userId:req.user.id})
    .then( category =>{
        res.send(category)
    })
    .catch(err=>res.send(err))
}


module.exports.viewSingleCategory = (req,res,next) => {
	
    Category.findById(req.params.id)
    .then( category =>{
        res.send(category)
    })
    .catch(next)
}



module.exports.updateCategory = (req,res,next) => {

	// let updatedCategory = {
	// 	category: req.body.category,
	// 	description : req.body.description
	// }
	// Category.findByIdAndUpdate(req.params.id, updatedCategory) 
	// .then( () => res.send(true) )
	// .catch( () => res.send(false))

	Category.findByIdAndUpdate(req.params.id, req.body, {new: true})
    .then( category =>{
        res.send(category)
    })
    .catch(next)

}





module.exports.deleteCategory = (req,res) => {
	Category.findByIdAndDelete(req.params.id)
	.then((category) => {
		res.send(category)
	})
	.catch((err)=>{
		res.send(err)
	})
}


