
const bcrypt = require('bcrypt');
const User = require('./../models/userModel')
const saltRounds = parseInt(process.env.SALT_ROUNDS)
const privateKey = process.env.PRIVATE_KEY
// const { createAccessToken } = require("./../auth")

const jwt = require ('jsonwebtoken')





module.exports.register = (req,res,next) => {
	let { password, confirmPassword } = req.body
	if ( password.length < 8 || password !== confirmPassword) {
		throw new Error("Password must be atleast 8 characters and must matched confirm password")
	} 

	bcrypt.hash(req.body.password, saltRounds)
	.then( hash => {
		req.body.password = hash

		return User.create(req.body) 
	})
	.then( user => {
       	user.password = undefined
        res.send(user)
	})
	.catch(next)

}





module.exports.login = (req,res,next) => {
	// User.findOne({ email : req.body.email})
	// 	.then(user => {
	// 		if(!user){
	// 			res.send(false)
	// 		}else{
	// 			let matchedPW = bcrypt.compareSync(req.body.password, user.password)
	// 			if(!matchedPW){
	// 				res.send(false)
	// 			}else{
	// 				res.send({ token : createAccessToken(user)})
	// 			}
	// 		}
	// 	})
	// 	.then( token =>{
 //        	res.send({token})
 //        })
	// 	.catch( err => {
	// 		console.log('catch', err)
	// 	})



	User.findOne({email : req.body.email})
    .then( user => {
        if (user) {
            req.user = user
            return user
        } else {
            res.status(401)
            next(new Error("Invalid credentials"))
        }
    })
    .then( user => {
        return bcrypt.compare(req.body.password, user.password)
    })
    .then( isMatchPassword => {
        if (isMatchPassword){
            
            return jwt.sign({ userId: req.user._id}, privateKey)

        } else {
            res.status(401)
            next(new Error("Invalid credentials"))
        }
    })
    .then( token =>{
        res.send({token})
    })
    .catch( next )

}





module.exports.getDetails = (req,res,next) => {

	res.send(req.user)

	// User.findById(req.user.id, {password :0} )
	// .then (user => {
	// 	res.send(user)
	// })
	// .catch( err => {
	// 	res.send(err)
	// })

}












