const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const cors = require('cors')
require('dotenv').config()


//connect to database:
require('./database');



// initialize express
const app = express();
// set port
const port = process.env.PORT || 4000;


app.use(helmet())
app.use(cors())
app.use(express.json())
app.use(morgan('dev'))




// initialize and defining the routes
const userRoutes = require('./routes/userRoutes');
app.use('/api/user', userRoutes)


const entryRoutes = require('./routes/entryRoutes');
app.use('/api', entryRoutes)


const categoryRoutes = require('./routes/categoryRoutes');
app.use('/api', categoryRoutes)




app.use((err,req,res,next)=>{
    if( res.statusCode == 200) res.status(400)
    res.send({
        error: {
             message: err.message
        }
    })
})




app.listen( port, ()=>{
	console.log(`Listening on port ${port}`)
})
