const mongoose = require('mongoose');

const EntrySchema = new mongoose.Schema({
	category: {
		type : String, 
		required: true
	},
	transaction: {
		type : String, // expenses or income ?
		required: true
	},
	amount: {
		type : String,
		required: true
	},
	userId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	}
},{timestamps : true})



module.exports = mongoose.model('Entry', EntrySchema)