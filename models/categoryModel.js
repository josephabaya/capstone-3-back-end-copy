const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
	category: {
		type: String,
		required : true	
	},
	description: {
		type: String,
		required : true	
	},
	userId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	}
  //   },
		// isActive: {
		// type: Boolean,
		// default: true
	// Amount: {
	// 	type: String,
	// 	required : true	
	// }
},{timestamps : true})





module.exports = mongoose.model('Category', CategorySchema)

