const express = require('express')
const router = express.Router()

const jwt = require ('jsonwebtoken')

const {
	register,
	login,
	getDetails
} = require ('./../controller/userController')

const { verifyUser } = require('./../auth') 
// might bu used in entryController or categoryController


//Guest Register Task 
router.post('/register', register);

// User login
router.post('/login', login);


router.get('/', verifyUser, getDetails);



module.exports = router;



















