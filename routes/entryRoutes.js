const router = require('express').Router()


const {
	addTransaction,
	getAllExpenses,
	getSingleExpenses,
	getAllIncome,
	getSingleIncome,
	updatedTransaction,
	deleteEntry
} = require('./../controller/entryController');


const { verifyUser } = require('./../auth')


// Add Transaction
router.post('/user/transaction', verifyUser, addTransaction);



// // get all expense
router.get('/user/expenses', verifyUser, getAllExpenses); //id = userId

// // get Single expense
router.get('/user/expenses/:id', verifyUser, getSingleExpenses);


// // get all income
router.get('/user/income', verifyUser, getAllIncome);

// // get Single income
router.get('/user/income/:id', verifyUser, getSingleIncome);



//put- update Transaction
router.put('/user/transaction/:id', verifyUser, updatedTransaction);

// //delete - expense or income
router.delete('/user/entry/:id', verifyUser, deleteEntry);





module.exports = router;