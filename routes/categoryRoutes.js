const router = require('express').Router()

const {
	addCategory,
	viewAllCategory,
	viewSingleCategory,
	updateCategory,
	deleteCategory
} = require ('./../controller/categoryController')

const { verifyUser } = require('./../auth')



router.post('/user/category', verifyUser, addCategory)

router.get('/user/category', verifyUser, viewAllCategory)

router.get('/user/category/:id', verifyUser, viewSingleCategory)

router.put('/user/category/:id', verifyUser, updateCategory)

router.delete('/user/category/:id', verifyUser, deleteCategory)




module.exports = router;