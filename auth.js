const e = require ('express');
const jwt = require ('jsonwebtoken')
const privateKey = process.env.PRIVATE_KEY
const User = require('./models/userModel')

module.exports.verifyUser = (req,res,next) => {
    let authorization = req.headers.authorization
    if(!authorization) {
        res.status(403)
        throw { message: "Unauthorize"}
    }

    let token = authorization.slice(7,authorization.length)
    
    let decoded = jwt.verify(token, privateKey)

    User.findById(decoded.userId).select({password:0})
    .then( user => {
        if (!user) {
            res.status(403)
            next(new Error("Unauthorize"))
        } else{
            req.user = user
            next()
        }
    })
    .catch(next)

}

module.exports.verifyAdmin = (req,res,next) =>{
    if (!req.user.isAdmin){
        throw {message: "Unauthorize"}
    } else {
        next()
    }
}






































// module.exports.createAccessToken = (user) => {
// 	let payload = {
// 		id : user._id,
// 		email : user.email
// 	}
// 	return jwt.sign(payload, privateKey)
// }



// module.exports.verifyUser = (req,res,next) => {
// 	let token = req.headers.authorization;

// 	if(typeof token === "undefined"){
// 		res.send({auth : "failed"})
// 	}else{
// 		token = token.slice(7, token.length)
// 		console.log(token)
// 	}

// 	jwt.verify(token, privateKey, function(err,decoded) {
// 		if(err){
// 			res.send({auth :"failed"})
// 		}else{
// 			req.user = decoded
// 			next()
// 		}
// 	});

// }



